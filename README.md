# Homework 1

## Step 1

- Clone git repository 

![](https://i5.imageban.ru/out/2023/08/20/89fb5aca11eedccf0b535cf7e1ad9221.png)

## Step 2
Cd to project repository

![](https://i3.imageban.ru/out/2023/08/20/2f4c5ead9c45454d856eee155617cf6f.png)

## Step 3
- Branch main and Update README.md 

![](https://i4.imageban.ru/out/2023/08/20/ebf110185d35a03c8e8e620ed407ec09.png)
```
git branch -M main
git add .
```
## Step 4
- Commit and push

![](https://i7.imageban.ru/out/2023/08/20/e1c384bf4f0fac994a5c5d3d35c16539.png)
```
git commit -m "Changed README"
git push
```
## Step 5
- Create new project
- Push new project to repository

![](https://i5.imageban.ru/out/2023/08/20/9ac1b855d4b4334dc35b09a822241415.png)

## Step 6
- Create new branch

![](https://i3.imageban.ru/out/2023/08/20/ab9dfb19a0c906d162cc412d28de0c38.png)
```
git checkout -b task-0001
```
- Create new class to project
- Commit branch
```
git commit -m "Add new helper class" -m "Class print text green color"
```
- Push branch
```
git push --set-upstream origin task-0001
```
- Merge branch task-0001 with main
- Update README and push

## Git commands

* `clone` -> скопировать репозиторий на свой локальный компьютер
* `add` -> отследить изменения в файлах (add .)
* `commit -m` -> сохранить изменения локально (-m "README" -m "Add new description")
* `push` -> перенести изменения в удаленный репозиторий
* `pull` -> перенести изменения из удаленного репозитория на свой локальный компьютер
* `status` -> проверить, какие файлы отслеживаются или нуждаются в индексации
* `init` -> используем эту команду внутри проекта, чтобы превратить его в репозиторий Git
* `branch -d` -> удалить ветку (branch -d task-0001)
* `checkout -b` -> создать новую ветку (checkout -b task-0001)
* `checkout` -> перейти на нужную ветку (checkout task-0001)
* `diff` -> посмотреть изменения, которые были внесены
* `merge` -> слияние веток (merge task-0002-conflict)
* `reset` -> отменить изменения (reset HEAD~1 отменить последний commit)
* `log` -> посмотреть журнал последних коммитов
